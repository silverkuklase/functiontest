// 1. Deploys as dbUsersOn-UserSignUp
 const functions = require('firebase-functions')
 const admin = require('firebase-admin')
const http = require("https");



console.log('onsignup', 'running');
 // // 2. Admin SDK can only be initialized once (You do that because the admin SDK can only be initialized once.)
try {admin.initializeApp(functions.config().firebase)} catch(e) {
       console.log('dbCompaniesOnUpdate initializeApp failure')
}

 // 3. Google Cloud environment variable used:
 // firebase functions:config:set sendgrid.key="API-KEY-HERE"


const SENDGRID_API_KEY = functions.config().sendgrid.key;

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(SENDGRID_API_KEY);


 // 4. Watch for new users


exports = module.exports = functions.firestore
     .document('users/{userId}')
     .onCreate((snap, context) => {


       const userdoc = snap.data();

       console.log('userdoc before', userdoc);

           // send userdata to sendgrid contacts

           var useroptions = {
         "method": "POST",
         "hostname": "api.sendgrid.com",
         "port": null,
         "path": "/v3/contactdb/recipients",
         "headers": {
           "authorization": "Bearer " + SENDGRID_API_KEY,
           "content-type": "application/json"
         }
       };

       var req = http.request(useroptions, (res) => {
         var chunks = [];

         res.on("data", (chunk) => {
           chunks.push(chunk);
         });

         res.on("end", () => {
           var body = Buffer.concat(chunks);
           console.log(body.toString());
         });
       });

       req.write(JSON.stringify([ {
          email: userdoc.email,
           first_name: userdoc.first_name,
           last_name: userdoc.last_name
          } ]));
       req.end();
           // userdata end

       return sendEmail(userdoc);
       // perform desired operations ...
     });


function sendEmail(userdoc) {


  console.log('userdoc after', userdoc);
  console.log('email vanilla', userdoc.email);


      const mailOptions = {

                    from: 'PointerPlanner.com <account@pointerplanner.com>',
                    to: userdoc.email,

                    // uncommented since sendgrid template is added instead.
                    //subject:  'Welcome email',
                    //text: 'Welcome text',
                    //html: '<strong>Welcome html!!!</strong>',

                    // custom templates
                    templateId: 'd-1b6374bee63742f9a68af44c1dcef4f0',
                    substitutionWrappers: ['{{', '}}'],
                    substitutions: {
                    name: 'userduder'
                      // and other custom properties here
                    }
}


  return sgMail.send(mailOptions)
                      .then(() => console.log('Welcome confirmation email successful'))
                      .catch((error) => console.error('There was an error while sending the email:', error))

}
